local g = vim.g
local o = vim.o

-- disable netrw at the very start of your init.lua (strongly advised)
g.loaded_netrw          = 1
g.loaded_netrwPlugin    = 1


-- Set leader key
-- g.mapleader = ','

--
-- General settiongs
--
o.clipboard     = 'unnamedplus'     -- Use system clipboard
o.backup        = false             -- Disable buffer backup at overwrite
o.writebackup   = false             -- Disable buffer backup at overwrite

o.undofile      = false             -- Don't write undos to file
o.swapfile      = false             -- Don't use buffer swap file

--
-- Better editor UI
--
o.number            = true          -- Show line number
o.relativenumber    = false         -- Disable relative line number

o.numberwidth       = 5             -- Spaces used by the line number
o.signcolumn        = 'yes'         -- No idea what it does

o.splitright        = true          -- Enable vertival split
o.splitbelow        = true          -- Enable horizontal split

o.termguicolors     = true          -- Enable 24 bites colors

--
-- Better editing experience
--
o.expandtab     = true              -- Use spaces instead of tab
o.smartindent   = true              -- Auto indent new line
o.shiftwidth    = 4                 -- Shift 4 spaces when tab
o.tabstop       = 4                 -- 1 tab == 4 spaces

o.ignorecase    = true              -- Ignore case letters when search
o.smartcase     = true              -- Ignore lowercase for the whole pattern
o.hlsearch      = true              -- Highlight search matches

o.wrap          = false             -- Do not wrap text
o.fileformat    = 'unix'            -- Set line endings to linux

o.fileencoding  = 'UTF-8'           -- The encoding written to file
o.autoread      = true              -- Reload file on change

--
-- Show spaces and tabs
--
vim.opt.list = true                 -- Enable space/tabs drawing
vim.opt.listchars:append 'tab:»·'   -- Character for tabs
vim.opt.listchars:append 'space:⋅'  -- Character for spaces

--
-- Fixes
--
g.powerline_loaded = 1              -- Powerline show
