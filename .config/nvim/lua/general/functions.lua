-- Deletes all trailing whitespaces
function _G.trim_trailing_whitespaces()
    if not vim.o.binary and vim.o.filetype ~= 'diff' then
        local current_view = vim.fn.winsaveview()
        vim.cmd([[keeppatterns %s/\s\+$//e]])
        vim.fn.winrestview(current_view)
    end
end

-- Trim trailing whitespace on save
local group = vim.api.nvim_create_augroup('trailing_whitespaces', { clear = true })
vim.api.nvim_create_autocmd('BufWritePre', { group = group, pattern = { '*' }, callback = trim_trailing_whitespaces })
