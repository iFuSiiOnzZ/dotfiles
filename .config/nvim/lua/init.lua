
-- General neovim settings
require('general')

-- Keyboard mappings for buffer and neovim
require('mappings')

-- Neovim plugins configuration
require('plugins')
