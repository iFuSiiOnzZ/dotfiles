-- Automatic donwload packer if not available
local function fnc_ensure_packer()
    local fn = vim.fn
    local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
        vim.cmd [[packadd packer.nvim]]
        return true
    end

    return false
end

local g_ensure_packer = fnc_ensure_packer()

local function fnc_set_up(use)
    -- Have Packer manage itself
    use { 'wbthomason/packer.nvim' }

    --
    -- UI themes
    --
    use { 'sainnhe/gruvbox-material' }
    use { 'stevearc/dressing.nvim' }
    use { 'rebelot/kanagawa.nvim' }
    use { 'folke/tokyonight.nvim' }
    use { 'rcarriga/nvim-notify' }
    use { 'windwp/windline.nvim' }

    --
    -- Language server protocol
    --
    use { 'p00f/clangd_extensions.nvim', requires = {'neovim/nvim-lspconfig'} }
    use { 'rust-lang/rust.vim' }
    use { 'preservim/tagbar' }

    --
    -- Debuggers
    --
    use { 'rcarriga/nvim-dap-ui', requires = {'mfussenegger/nvim-dap', 'nvim-neotest/nvim-nio'} }
    use { 'folke/trouble.nvim', requires = {'nvim-tree/nvim-web-devicons'} }

    --
    -- Autocomplete secction
    --
    use { 'hrsh7th/nvim-cmp', requires = {'hrsh7th/cmp-nvim-lsp', 'hrsh7th/cmp-vsnip', 'hrsh7th/vim-vsnip'} }
    use { 'folke/neodev.nvim' }

    --
    -- Code utilities
    --
    use { 'williamboman/mason.nvim' }
    use { 'mhartington/formatter.nvim' }
    use { 'danymat/neogen', requires = {'nvim-treesitter/nvim-treesitter'} }
    use { 'https://gitlab.com/iFuSiiOnzZ/nvim-cmake.git', as = 'nvim-cmake' }

    --
    -- File explorer and searcher
    --
    use { 'nvim-telescope/telescope.nvim', branch = 'master', requires = {'nvim-lua/plenary.nvim'} }
    use { 'kyazdani42/nvim-tree.lua', requires = {'kyazdani42/nvim-web-devicons'} }
    use { 'nvim-pack/nvim-spectre', requires = {'nvim-lua/plenary.nvim'} }
    use { 'nvim-telescope/telescope-ui-select.nvim' }

    --
    -- Better highlights
    --
    use { 'nvim-treesitter/nvim-treesitter', run = function() pcall(require('nvim-treesitter.install').update {with_sync = true}) end }
    use { 'folke/todo-comments.nvim', requires = {'nvim-lua/plenary.nvim'} }
    use { 'NvChad/nvim-colorizer.lua' }
    use { 'itchyny/vim-highlighturl' }
    use { 'RRethy/vim-illuminate' }

    --
    -- Other usefull plugins
    --
    use { 'j-hui/fidget.nvim', opts = {} }
    use { 'f-person/git-blame.nvim' }
    use { 'echasnovski/mini.icons' }
    use { 'folke/which-key.nvim' }
    use { 'jghauser/mkdir.nvim' }
    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
    if g_ensure_packer then
        require('packer').sync()
    end
end

return require('packer').startup(fnc_set_up)
