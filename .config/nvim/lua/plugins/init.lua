-- Packer plugin manager
require('plugins/packer')

require('plugins/themes/colorschema')

require('plugins/code/treesitter')
require('plugins/code/formatter')
require('plugins/code/dap')
require('plugins/code/lsp')

require('plugins/tools/todo-comments')
require('plugins/tools/nvim-tree')
require('plugins/tools/telescope')
require('plugins/tools/trouble')
require('plugins/tools/mason')
require('plugins/tools/cmake')
require('plugins/tools/other')
