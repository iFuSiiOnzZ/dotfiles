local status_ok, tc = pcall(require, 'todo-comments')
if not status_ok then
    return
end

local opts =
{
    gui_style =
    {
        fg = 'NONE',
        bg = 'NONE'
    },

    highlight =
    {
        pattern = [[.*<(KEYWORDS)\s*(\(.*\))?\s*:]],
        keyword = 'fg',
        after = 'fg'
    },

    search =
    {
        pattern = [[\b(KEYWORDS)\s*(\(.*\))?\s*:]]
    }
}

tc.setup(opts)
