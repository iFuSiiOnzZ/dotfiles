local status_ok_colorizer, colorizer = pcall(require, 'colorizer')
if status_ok_colorizer then
    colorizer.setup()
end

local status_ok_illuminate, illuminate = pcall(require, 'illuminate')
if status_ok_illuminate then
    illuminate.configure()
end

local status_ok_dressing, dressing = pcall(require, 'dressing')
if status_ok_dressing then
    dressing.setup()
end

local status_ok_which_key, which_key = pcall(require, 'which-key')
if status_ok_which_key then
    vim.o.timeoutlen = 300
    which_key.setup()
end

local status_ok_spectre, spectre = pcall(require, 'spectre')
if status_ok_spectre then
    spectre.setup()
end

local status_ok_windline, windline = pcall(require, 'windline')
if status_ok_windline then
    require('wlsample.vscode')
end

local status_ok_fidget, fidget = pcall(require, 'fidget')
if status_ok_fidget then
    fidget.setup {}
end

local status_ok_neogen, neogen = pcall(require, 'neogen')
if status_ok_neogen then
    neogen.setup({})
end
