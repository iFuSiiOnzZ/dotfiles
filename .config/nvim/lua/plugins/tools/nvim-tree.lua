local status_ok, nvim_tree = pcall(require, 'nvim-tree')
if not status_ok then
    return
end

nvim_tree.setup
{
    actions =
    {
        open_file =
        {
            quit_on_open = true,
        }
    },

    update_focused_file =
    {
        enable = true,
        update_cwd = true
    },

    view =
    {
        adaptive_size = true
    },

    renderer =
    {
        group_empty = true
    },

    filters =
    {
        dotfiles = true
    }
}


vim.api.nvim_create_autocmd({'QuitPre'}, {
    callback = function() vim.cmd('NvimTreeClose') end,
})

local function opts(desc)
    return { desc = 'nvim-tree: ' .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
end

local api = require('nvim-tree.api')
vim.keymap.set('n', '<S-f>', api.tree.toggle, opts('Show/Hide file tree'))
