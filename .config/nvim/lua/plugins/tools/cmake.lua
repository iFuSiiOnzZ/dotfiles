local status_ok, cmake = pcall(require, 'nvim-cmake')
if not status_ok then
    return
end

local utils = require('nvim-cmake.utils')
local number_of_cores = #vim.loop.cpu_info() - 4
number_of_cores = (number_of_cores < 1) and 1 or number_of_cores

local function fnc_on_success(code, signal, type)
    local msg = 'Command finished sucessful (type/code/signal): (' .. type .. '/' .. code .. '/' .. signal .. ')'
    vim.notify(msg, vim.log.levels.INFO, {title = 'CMake'})
    cmake.hide()
end

local function fnc_on_failure(code, signal, type)
    local msg = 'Command finished with error (type/code/signal): (' .. type .. '/' .. code .. '/' .. signal .. ')'
    vim.notify(msg, vim.log.levels.ERROR, {title = 'CMake'})
end

local function fnc_has_cmakelists_file()
    local f = utils.combine_path(vim.loop.cwd(), 'CMakeLists.txt')
    return utils.file_exists(f)
end

local function fnc_on_enter()
    if fnc_has_cmakelists_file() then
        cmake.generate()
    end
end

local function fnc_on_leave()
    if fnc_has_cmakelists_file() then
        local cache_dir = utils.combine_path(vim.loop.cwd(), '.cache')
        utils.remove(cmake.dir())
        utils.remove(cache_dir)
    end
end

local setup =
{
    executable =
    {
        -- cmake path or accessible through the environment
        cmd = 'cmake',

        -- additional arguments for the generation of cmake projects (CMAKE_BUILD_TYPE is set by the plugin)
        cmake_args = { '-G', 'Unix Makefiles', '-D', 'CMAKE_EXPORT_COMPILE_COMMANDS=1' },

        -- additional arguments for executing the build command
        build_args = { '-j', number_of_cores }
    },

    output =
    {
        -- build type should be: Debug, Release, etc...
        --
        build_type = 'Debug',

        -- cmake directory output
        -- if empty -> build_directory = build_directory_prefix + build_type
        build_directory = '',

        -- when cmake_build_directory is '', this option will be used
        -- build_directory = build_directory_prefix + build_type
        build_directory_prefix = 'cmake-build-'
    },

    console =
    {
        -- hight of the output console
        size = 15,

        -- console position (botright/topleft)
        position = 'botright'
    },

    callbacks =
    {
        -- Callback after opening nvim (setup done)
        on_enter = fnc_on_enter,

        -- Callback before closing nvim
        on_leave = fnc_on_leave,

        -- Callback command is successful
        on_success = fnc_on_success,

        -- Callback command has failed
        on_failure = fnc_on_failure
    }
}

cmake.setup(setup)
-- Use Alt-B build the cmake project
vim.keymap.set('n', '<A-b>', cmake.build, { desc = 'Build the project' })

-- Use Alt-C clean the cmake project
vim.keymap.set('n', '<A-c>', cmake.clean, { desc = 'Clean build objects' })

-- Use Alt-G genrate the cmake project
vim.keymap.set('n', '<A-g>', cmake.generate, { desc = 'Generate CMake solution' })

