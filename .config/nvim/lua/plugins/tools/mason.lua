local status_meson_ok, meson = pcall(require, 'mason')
if not status_meson_ok then
    return
end

meson.setup()
