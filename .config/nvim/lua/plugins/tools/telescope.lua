local status_ok, telescope = pcall(require, 'telescope')
if not status_ok then
    return
end

telescope.setup
{
    defaults =
    {
        path_display = { 'smart' },
        file_ignore_patterns = { '.git/' }
    },

    pickers =
    {
        find_files =
        {
            find_command = { 'fd', '--type', 'f', '--color', 'never', '--no-require-git' }
        }
    }
}

local status_ok_ui_selector, _ = pcall(require, 'ui-select')
if status_ok_ui_selector then
    telescope.setup
    {
        extensions =
        {
            ["ui-select"] =
            {
                require("telescope.themes").get_dropdown {}
            }
        }
    }

    telescope.load_extension("ui-select")
end

local keymap = vim.api.nvim_set_keymap
local function opts(descrption) return { noremap = true, desc = descrption } end

keymap('n', '<space>ff', "<cmd>lua require('telescope.builtin').find_files()<cr>"   , opts("Find files"))
keymap('n', '<space>fg', "<cmd>lua require('telescope.builtin').live_grep()<cr>"    , opts("Find in files"))
keymap('n', '<space>fb', "<cmd>lua require('telescope.builtin').buffers()<cr>"      , opts("Find open buffers"))
keymap('n', '<space>fh', "<cmd>lua require('telescope.builtin').help_tags()<cr>"    , opts("Show help tags"))

keymap('n', '<space>tws', "<cmd>lua require('telescope.builtin').lsp_workspace_symbols()<cr>"   , opts("Find workspace symbols"))
keymap('n', '<space>tds', "<cmd>lua require('telescope.builtin').lsp_document_symbols()<cr>"    , opts("Find document symbols"))
keymap('n', '<space>tic', "<cmd>lua require('telescope.builtin').lsp_incoming_calls()<cr>"      , opts("Find incoming calls"))
keymap('n', '<space>tio', "<cmd>lua require('telescope.builtin').lsp_outgoing_calls()<cr>"      , opts("Find outgoing calls"))
keymap('n', '<space>ti', "<cmd>lua require('telescope.builtin').lsp_implementations()<cr>"      , opts("Find implementations"))
keymap('n', '<space>td', "<cmd>lua require('telescope.builtin').lsp_definitions()<cr>"          , opts("Find definistions"))
keymap('n', '<space>tr', "<cmd>lua require('telescope.builtin').lsp_references()<cr>"           , opts("Find references"))
keymap('n', '<space>tx', "<cmd>lua require('telescope.builtin').diagnostics()<cr>"              , opts("Show diagnostics"))
