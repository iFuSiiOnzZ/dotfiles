local status_ok_notify, notify = pcall(require, 'notify')
if status_ok_notify then
    vim.notify = notify
end

local theme = 'kanagawa' -- 'gruvbox-material' 'tokyonight-night'
local status_ok_scheme, _ = pcall(vim.cmd, 'colorscheme ' .. theme)

if not status_ok_scheme then
    vim.notify(theme .. ' theme can not be set', vim.log.levels.WARN)
end
