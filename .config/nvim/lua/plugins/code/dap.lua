-- Debugging configuration
local status_ok, dap = pcall(require, 'dap')
if not status_ok then
    return
end

-- Get command full path (uses where [win32] and which [linux])
local function fnc_get_full_path(cmd)
    if vim.fn.executable(cmd) ~= 1 then
        return cmd
    end

    local is_win = package.config:sub(1,1) == '\\'
    local find = is_win and 'where.exe' or 'which'

    local f = io.popen(find .. ' ' .. cmd, 'r')
    if f == nil then return cmd end

    local s = f:read('*a')
    f:close()

    return s:gsub('^%s*(.-)%s*$', '%1')
end

-- Plaform directory separator
local DS = package.config:sub(1,1)

-- Set debugger type
local debugger = nil

vim.keymap.set('n', '<F9>' , dap.toggle_breakpoint)
vim.keymap.set('n', '<F5>' , dap.continue)
vim.keymap.set('n', '<F10>', dap.step_over)
vim.keymap.set('n', '<F11>', dap.step_into)
vim.keymap.set('n', '<F12>', dap.step_out)

dap.adapters.lldb =
{
    command = fnc_get_full_path('lldb-vscode'),
    type = 'executable',
    name = 'lldb'
}

dap.adapters.codelldb =
{
    type = 'server',
    port = 32064,

    executable =
    {
        command = fnc_get_full_path('codelldb'),
        args = { '--port', '32064' },

        -- On windows you may have to uncomment this (detached = false):
        detached = not vim.fn.has('win32')
    }
}

dap.adapters.cppdbg =
{
    id = 'cppdbg',
    type = 'executable',
    command = fnc_get_full_path('OpenDebugAD7'),

    options =
    {
        -- On windows you may have to uncomment this (detached = false):
        detached = not vim.fn.has('win32')
    }
}

if vim.fn.executable('OpenDebugAD7') == 1 then
    debugger = 'cppdbg'
elseif vim.fn.executable('codelldb') == 1 then
    debugger = 'codelldb'
elseif vim.fn.executable('lldb-vscode') == 1 then
    debugger = 'lldb'
end

dap.configurations.cpp =
{
    {
        type = debugger,
        name = 'Launch',
        request = 'launch',

        program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. DS, 'file')
        end,

        cwd = vim.fn.getcwd(),
        stopOnEntry = false,

        setupCommands =
        {
            {
                ignoreFailures = false,
                text = '-enable-pretty-printing',
                description = 'enable pretty printing'
            }
        }
    }
}

dap.configurations.rust = dap.configurations.cpp
dap.configurations.c = dap.configurations.cpp

local status_ok_dapui, dapui = pcall(require, 'dapui')
if not status_ok_dapui then
    return
end

dapui.setup()
vim.keymap.set('n', '<F8>' , dapui.eval)

dap.listeners.before.attach.dapui_config = function()
  dapui.open()
end

dap.listeners.before.launch.dapui_config = function()
  dapui.open()
end

dap.listeners.before.event_terminated.dapui_config = function()
  dapui.close()
end

dap.listeners.before.event_exited.dapui_config = function()
  dapui.close()
end
