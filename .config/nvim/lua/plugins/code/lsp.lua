-- Neovim lua configuration (required before lspconfig)
local status_ok_neodev, neodev = pcall(require, 'neodev')
if status_ok_neodev then
    neodev.setup(
        {
            library =
            {
                plugins = { 'nvim-dap-ui' },
                types = true
            },
        }
    )
end


-- Completion plugin
local status_ok_cmp, cmp = pcall(require, 'cmp')
if status_ok_cmp then
    cmp.setup
    {
        mapping =
        {
            ['<A-q>'] = cmp.mapping.abort(),
            ['<CR>'] = cmp.mapping.confirm({ select = 'false' }),

            ['<Up>'] = cmp.mapping.select_prev_item({}),
            ['<Down>'] = cmp.mapping.select_next_item({}),
            ['<C-Space>'] = cmp.mapping.complete()
        },

        sources =
        {
            { name = 'nvim_lsp' },
            { name = 'vsnip' }
        },

        snippet =
        {
            expand = function(args)
                vim.fn['vsnip#anonymous'](args.body)
            end
        },

        sorting =
        {
            comparators =
            {
                 require('clangd_extensions.cmp_scores'),
            }
        }
    }
end

-- Language server protocol configuration
local status_ok_lsp, lspconfig = pcall(require, 'lspconfig')
if status_ok_lsp then
    lspconfig.lua_ls.setup
    {
        settings =
        {
            Lua =
            {
                runtime =
                {
                    version = 'LuaJIT'
                },

                diagnostics =
                {
                    globals = {'vim', 'nvim', 'global'}
                },

                workspace =
                {
                    library = vim.api.nvim_get_runtime_file('', true),
                    checkThirdParty = false
                },

                completion =
                {
                    callSnippet = 'Replace'
                },

                telemetry =
                {
                    enable = false
                }
            }
        }
    }

   lspconfig.rust_analyzer.setup
   {
        --on_attach = function(client, bufnr)
        --    vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
        --end,

        settings =
        {
            ['rust-analyzer'] =
            {
                diagnostics =
                {
                    enable = false;
                }
            }
        }
    }

    lspconfig.clangd.setup
    {
        on_new_config = function(new_config, new_cwd)
            local status, cmake = pcall(require, 'nvim-cmake')
            if status then cmake.clangd(new_config) end
        end,
    }

    lspconfig.csharp_ls.setup{}
end

local on_attach_callback = function(ev)
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    local function bufopts(description)
        return { noremap = true, silent = true, buffer = ev.buffer, desc = description }
    end

    vim.keymap.set('n', '<space>gD' , vim.lsp.buf.definition                , bufopts("Go to definition"))
    vim.keymap.set('n', '<space>gd' , vim.lsp.buf.declaration               , bufopts("Go to declaration"))
    vim.keymap.set('n', '<space>gi' , vim.lsp.buf.implementation            , bufopts("Go to implementation"))

    vim.keymap.set('n', '<space>fr' , vim.lsp.buf.references                , bufopts("Find references"))
    vim.keymap.set('n', '<space>rr' , vim.lsp.buf.rename                    , bufopts("Rename"))

    vim.keymap.set('n', '<space>wa' , vim.lsp.buf.add_workspace_folder      , bufopts("Add workspace folder"))
    vim.keymap.set('n', '<space>wr' , vim.lsp.buf.remove_workspace_folder   , bufopts("Remove workspace folder"))

    vim.keymap.set('n', '<space>do' , vim.diagnostic.open_float             , bufopts("Open float"))
    vim.keymap.set('n', '<space>dq' , vim.diagnostic.setloclist             , bufopts("Set loc list"))

    vim.keymap.set('n', '<spane>dp' , vim.diagnostic.goto_prev              , bufopts("Go to prev"))
    vim.keymap.set('n', '<space>dn' , vim.diagnostic.goto_next              , bufopts("Go to next"))

    vim.keymap.set('n', '<space>fh' , vim.lsp.buf.signature_help            , bufopts("Show signature help"))
    vim.keymap.set('n', '<space>lf' , vim.lsp.buf.format                    , bufopts("Format file"))
    vim.keymap.set('n', '<space>h'  , vim.lsp.buf.hover                     , bufopts("Hover"))

    vim.keymap.set('n', '<space>ca' , vim.lsp.buf.code_action               , bufopts("Show code actions"))
end

local on_attach =
{
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
    callback = on_attach_callback
}

vim.api.nvim_create_autocmd('LspAttach', on_attach)
