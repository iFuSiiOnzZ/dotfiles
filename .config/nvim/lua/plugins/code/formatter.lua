local status_ok_formatter, formatter = pcall(require, 'formatter')
if status_ok_formatter then
    formatter.setup {
        logging = true,
        log_level = vim.log.levels.WARN,

        filetype =
        {
            c =
            {
                require("formatter.filetypes.c").clangformat,
            },

            cpp =
            {
                require("formatter.filetypes.cpp").clangformat,
            },

            lua =
            {
                require("formatter.filetypes.lua").stylua,
            },

            json =
            {
                require("formatter.filetypes.json").fixjson
            }
        }
    }
end
