local status_ok, configs = pcall(require, 'nvim-treesitter.configs')
if not status_ok then
    return
end

configs.setup
{
    ensure_installed = { '' },
    ignore_install = { '' },

    highlight =
    {
        enable = true,
        disable = {}
    },

    rainbow =
    {
        enable = true,
        extended_mode = true
    },

    autopairs =
    {
        enable = true
    },

    indent =
    {
        enable = true,
        disable = {}
    }
}
