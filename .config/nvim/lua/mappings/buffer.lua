local function map(mode, key, cmd)
    vim.keymap.set(mode, key, cmd, { silent = true })
end

-- Save to file
map('n', '<C-s>', ':w<CR>'      )
map('i', '<C-s>', '<ESC>:w<CR>a')

-- Move between paragraphs
map('n', '<C-Up'    , '<M-{>')
map('n', '<C-Down'  , '<M-}>')

-- Move line up and down in NORMAL and VISUAL modes (Reference: https://vim.fandom.com/wiki/Moving_lines_up_or_down)
map('n', '<S-Down>' , ':move .+1<CR>')
map('n', '<S-Up>'   , ':move .-2<CR>')
map('i', '<S-Down>' , '<ESC>:move .+1<CR>a')
map('i', '<S-Up>'   , '<ESC>:move .-2<CR>a')
