local function map(mode, key, cmd)
    vim.keymap.set(mode, key, cmd, { silent = true })
end

-- Quit neovim
map('n', '<C-q>', ':q!<CR>'     )
map('i', '<C-q>', '<ESC>:q!<CR>')

-- Splits
map('n', '<A-v>', ':vs<CR>')
map('n', '<A-h>', ':sp<CR>')
