local wezterm = require 'wezterm'
local config = wezterm.config_builder()

if wezterm.target_triple == 'x86_64-pc-windows-msvc' then
    config.default_prog = { 'powershell.exe', '-NoLogo' }
end

config.font_size = 10
config.hide_tab_bar_if_only_one_tab = true

config.use_fancy_tab_bar = true
config.tab_bar_at_bottom = true

config.window_padding =
{
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
}

config.keys =
{
    {
        key = 'd',
        mods = 'CTRL',
        action = wezterm.action.CloseCurrentTab { confirm = true }
    },
}

return config
