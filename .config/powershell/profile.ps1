function Test-Administrator
{
    process
    {
        [Security.Principal.WindowsPrincipal]$user = [Security.Principal.WindowsIdentity]::GetCurrent();
        return $user.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator);
    }
}

function List-Dir()
{
    ls.exe --icons --colors --virterm --group-directories-first @args
}

function Dir-Up()
{
	Set-Location (Split-Path -Path (Get-Location).Path -Parent)
}

function UpdateAllModules()
{
    if (-not (Test-Administrator))
    {
        Write-Error "This function must be executed as Administrator.";
        return;
    }

    Get-InstalledModule | ForEach-Object {
        Write-Host "Update " $PSItem.Name
        Update-Module $PSItem.Name
    }
}

# Auto complete
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete

# Close terminal con CTRL+D
Set-PSReadlineKeyHandler -Chord Ctrl+d -Function DeleteCharOrExit

# Useful alias
Set-Alias -name 'ls' -value List-Dir -Option AllScope
Set-Alias -name '..' -value Dir-Up   -Option AllScope

# Modules available at startup
Import-Module posh-git
Invoke-Expression (&starship init powershell)
