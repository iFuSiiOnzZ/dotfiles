#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export EDITOR="micro"       # $EDITOR use micro in terminal
export VISUAL="subl"        # $VISUAL use siblime in GUI mode
export HISTFILE=/dev/null   # Don't export shell commands history

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth:erasedups

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# Dotfiles repo dir
DOTFILES=$HOME/.dotfiles

# SHOPTs
shopt -s expand_aliases # expand aliases
shopt -s checkwinsize # check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s histappend # append to the history file, don't overwrite it
shopt -s cmdhist # save multi-line commands in history as single line

# Colorize grep output
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Colorize dir output
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

# Userfull alias
alias '..'='cd ..'

# Pacman useful alias
alias system-clean='sudo pacman -Rns $(pacman -Qtdq)'
alias system-update='sudo pacman -Syu && paru -Syu'

# Change title of terminals
case ${TERM} in
    xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|alacritty|st|konsole*)
        PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
        ;;
    screen*)
        PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
        ;;
esac

# For 'dot-files' repository
if type 'git' > /dev/null; then
    function dtf()
    {
        git --git-dir="$DOTFILES" --work-tree="$HOME" "$@"
    }

    function dtf-new()
    {
        git init --bare "$DOTFILES"
        dtf config --local status.showUntrackedFiles no
    }

    function dtf-restore()
    {
        git clone --bare $1 "$DOTFILES"
        dtf config --local status.showUntrackedFiles no
        dtf checkout
    }

    if [ -d "$DOTFILES" ]; then
        dtf config --local status.showUntrackedFiles no
    fi
fi

# Make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Add user local "PATHs" to system "PATH"
if [ -d "$HOME/.bin" ]; then
    PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/.local/share/nvim/mason/bin" ]; then
	PATH="$HOME/.local/share/nvim/mason/bin:$PATH"
fi

# Set XDG base directory environment variables
if [ -z "$XDG_CONFIG_HOME" ]; then
    export XDG_CONFIG_HOME="$HOME/.config"
fi

if [ -z "$XDG_DATA_HOME" ]; then
    export XDG_DATA_HOME="$HOME/.local/share"
fi

if [ -z "$XDG_CACHE_HOME" ]; then
    export XDG_CACHE_HOME="$HOME/.cache"
fi

# Enable bash completion
if [ -f "/usr/share/bash-completion/bash_completion" ]; then
    source "/usr/share/bash-completion/bash_completion"
fi

# Replace 'ls' with 'exa'
if type 'exa' > /dev/null; then
    alias ls='exa --color=always --group-directories-first'
fi

# Change 'cat' command with 'bat' and use bat for manpager
if type 'bat' > /dev/null; then
    alias cat=bat
    export MANPAGER="sh -c 'col -bx | bat -l man -p'"
fi

# Show system info
if type 'neofetch' > /dev/null; then
    neofetch
fi

# Startship powerline
eval "$(starship init bash)"

# Create a new directory and enter it
function mkd()
{
    mkdir -p "$@" && cd "$_";
}

# Clean up history on terminal close
function history_clean_up()
{
    [ -f "$HOME/.bash_history" ] && rm -rf "$HOME/.bash_history"
    history -c
}
trap history_clean_up EXIT
